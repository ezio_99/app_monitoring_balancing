# Info

This is [Final project](https://gitlab.com/ezio_99/app_monitoring_balancing) for course of System and Network Administration hosted at Innopolis University at Fall semester, 2021.

## Goals / Tasks of the Project

Goals of this project learn how to configure monitoring for application using Prometheus and Grafana and using nginx balance load for several instances of this application.

The main goal of this project is to consolidate the existing knowledge gained in the course (NGINX, docker,
docker-compose, networks) and get new ones (Grafana, Prometheus).

## Demo

You can watch demo video by this [link](https://www.youtube.com/watch?v=Yvzohkc3FR4).

## Structure of the project

    .
    ├── app                         # app, which will be monitored
    │   ├── Dockerfile              # Dockerfile for app
    │   └── src                     # source code of app
    │       ├── app.py              # entry point for app
    │       ├── middleware.py       # middlewares
    │       └── requirements.txt    # requirements for app
    ├── config                      # all configs
    │   ├── grafana                 # all configs for grafana
    │   │   └── dashboard.json      # dashboard, which can be imported
    │   ├── nginx                   # all configs for nginx
    │   │   ├── Dockerfile          # Dockerfile for nginx
    │   │   ├── multi               # all configs for several backends setup
    │   │   │   └── nginx.conf      # config for several backends
    │   │   └── single              # all configs for single backend setup
    │   │       └── nginx.conf      # config  for single backend
    │   ├── prometheus              # all configs for prometheus
    │   │   ├── multi               # all configs for several backends setup
    │   │   │   └── prometheus.yml  # config for several backends
    │   │   └── single              # all configs for single backends setup
    │   │       └── prometheus.yml  # config for single backend
    │   └── yatank                  # all configs for yandex-tank
    │       ├── multi               # all configs for several backends
    │       │   └── load.yaml       # config for load testing with several backends
    │       └── single              # all configs for single backend
    │           └── load.yaml       # config for load testing with sing backend
    ├── docker-compose.multi.yml    # docker-compose for several backends
    ├── docker-compose.single.yml   # docker-compose for single backend
    └── README.md                   # description of the project

## How to run

Before running any of commands below you should create docker network with name `public`. This is necessary, because
nginx (and only nginx) connected to this network, and we can access it via this network.

    docker network create public

### Run infrastructure

Run infrastructure in single backend mode

    docker-compose -f docker-compose.single.yml up --build

Run infrastructure in several backends mode

    docker-compose -f docker-compose.multi.yml up --build

### Run load tests

Run load testing for single backend mode

    docker run -v $(pwd)/config/yatank/single:/var/loadtest --network public -it direvius/yandex-tank

Run load testing for several backends mode

    docker run -v $(pwd)/config/yatank/multi:/var/loadtest --network public -it direvius/yandex-tank

## Access to Grafana and backends

Both Grafana and backends can be reached only via nginx and standard HTTP port 80.

Default password for Grafana is `admin`. After first login you should change it. Also, you can change default password in `docker-compose.single.yml` or `docker-compose.multi.yml` (`GF_SECURITY_ADMIN_PASSWORD` env).

You can import dashboard, used for this project. For doing that you can either upload `config/grafana/dashboard.json` or paste it's content to json box on import page.

* Grafana
    * Main page &mdash; [http://localhost/grafana](http://localhost/grafana)
    * Import dashboard &mdash; [http://localhost/grafana/dashboard/import](http://localhost/grafana/dashboard/import)
* Backends
    * Test endpoint imitating CPU load &mdash; [http://localhost/cpu](http://localhost/cpu)
    * Test endpoint imitating IO load &mdash; [http://localhost/io](http://localhost/io)
    * Test endpoint imitating CPU and IO load &mdash; [http://localhost/cpu-io](http://localhost/cpu-io)

## Utilization of solution / Tests



### Division into networks

We have used 4 docker networks. Below they listed as:

* *network*
  * service 1
  * service 2

---

* *public*
  * nginx
  * any other **external** services or containers
* *grafana*
  * prometheus
  * grafana
* *prometheus*
  * prometheus
  * app0
  * app1
  * app2
* *nginx*
  * nginx
  * app0
  * app1
  * app2

### Single backend mode scheme
![](.attachments/single/scheme.png)

### Several backends mode scheme
![](.attachments/multi/scheme.png)

For testing both monitoring and load balancing we have used [Yandex Tank](https://github.com/yandex/yandex-tank). We can
check metrics via Grafana Web interface. Also, looking to graph for each application say us that load balancing works.

Results of testing both single and multi backend modes presented below.

### Testing of single backend mode

![](.attachments/single/graphs.png)

![](.attachments/single/stats.png)

### Testing of several backends mode

![](.attachments/multi/graphs.png)

![](.attachments/multi/stats.png)

## Difficulties faced

For implementing this project, we have learned how Prometheus works, which metric types it provides, how to query
metrics using PromQL. Knowing information before, it was easy to connect Prometheus to Grafana and setup dashboard with
graphs for different metrics sent by our application. For nginx, we searched for more speedy configuration and used it.
We already knew how to work with Docker and docker-compose, but for combining all these stuff together, we had to search
some additional information such as how to configure reverse proxy for Grafana (because we have never faced task with
configuring reverse proxy for websockets) or controlling resources for Docker containers.

## Conclusion, contemplations and judgement

While working on the project, we got acquainted with the monitoring tools represented by Prometheus and Grafana. In
addition, we summed up our knowledge of the course by developing a system of several applications running in docker
containers, as well as being balanced using Nginx.
