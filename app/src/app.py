import asyncio
import os
import random
import time

from aiohttp import web
from middleware import error_middleware, setup_metrics


def fake_cpu_load():
    time.sleep(random.randint(1, 5) / 100)


async def fake_io_load():
    await asyncio.sleep(random.randint(1, 10) / 100)


async def cpu(request):
    fake_cpu_load()
    return web.Response(text="cpu load")


async def io(request):
    await fake_io_load()
    return web.Response(text="io load")


async def cpu_io(request):
    fake_cpu_load()
    await fake_io_load()
    return web.Response(text="cpu and io load")


def main():
    app_name = os.environ["APP_NAME"]
    app = web.Application(middlewares=[error_middleware])
    setup_metrics(app, app_name)
    app.router.add_get("/cpu", cpu)
    app.router.add_get("/io", io)
    app.router.add_get("/cpu-io", cpu_io)
    web.run_app(app, port=8080)


if __name__ == "__main__":
    main()
